﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCI32Lib.Tools
{
    public static class PaletteTools
    {
        #region Microsoft Load Palette
        public static Color[] LoadMicrosoftPalette(byte[] buffer)
        {
            List<Color> colors = new List<Color>();
            MemoryStream stream = new MemoryStream(buffer);
            using (BinaryReader reader = new BinaryReader(stream))
            {
                // RIFF Header
                string riff = Encoding.ASCII.GetString(reader.ReadBytes(4)); // RIFF
                int dataSize = reader.ReadInt32();
                string type = Encoding.ASCII.GetString(reader.ReadBytes(4)); // PAL 

                // Data Chunk
                string chunkType = Encoding.ASCII.GetString(reader.ReadBytes(4)); // data
                int chunkSize = reader.ReadInt32();
                short palVersion = reader.ReadInt16();
                short palEntries = reader.ReadInt16();

                for (int i = 0; i < palEntries; i++)
                {
                    byte red = reader.ReadByte();
                    byte green = reader.ReadByte();
                    byte blue = reader.ReadByte();
                    byte flags = reader.ReadByte();
                    colors.Add(Color.FromArgb(red, green, blue));
                }
                return colors.ToArray();
            }
        }
        /// <summary>
        /// Saves a Color array of 256 colors as a Microsoft palette *.pal file
        /// </summary>
        /// <param name="filename">The *.pal file to save</param>
        /// <param name="colors">array of 256 colors</param>
        /// <param name="saveAlpha">true to use R, G, B, A, false to use R, G, B, 0</param>
        public static void SaveMicrosoftPalette(string filename, Color[] colors, bool saveAlpha = false)
        {
            int length = 4 + 4 + 4 + 4 + 2 + 2 + colors.Length * 4;

            FileStream stream = new FileStream(filename, FileMode.Create, FileAccess.Write,
                FileShare.None);
            using (BinaryWriter bw = new BinaryWriter(stream))
            {
                // RIFF Header
                bw.Write(Encoding.ASCII.GetBytes("RIFF"));
                bw.Write(length);
                bw.Write(Encoding.ASCII.GetBytes("PAL "));

                // Data Chunk
                bw.Write(Encoding.ASCII.GetBytes("data"));
                bw.Write(colors.Length * 4 + 4);
                bw.Write((short)0x0300);
                bw.Write((short)colors.Length);

                for (int i = 0; i < colors.Length; i++)
                {
                    bw.Write(colors[i].R);
                    bw.Write(colors[i].G);
                    bw.Write(colors[i].B);
                    if (saveAlpha) bw.Write(colors[i].A);
                    else bw.Write((byte)0);
                }
            }
        }
        #endregion
    }
}

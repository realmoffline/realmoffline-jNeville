﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
//using SCILib.Palettes;
using System.IO;

namespace SCI32Lib.Controls
{
    public partial class PaletteControl : UserControl
    {
        public delegate void PaletteLoaded(Color[] colors);
        public event PaletteLoaded OnPaletteLoaded;

        private Bitmap originalBitmap;
        public Bitmap OriginalBitmap
        {
            get { return originalBitmap; }
            //set { originalBitmap = value; }
        }
        private Dictionary<Rectangle, Color> palette;
        public Dictionary<Rectangle, Color> PaletteRectangles
        {
            get { return palette; }
        }
        public int PaletteWidth
        {
            get { return paletteBox.Width; }
            set { paletteBox.Width = value; }
        }
        public int PaletteHeight
        {
            get { return paletteBox.Height; }
            set { paletteBox.Height = value; }
        }
        private int cellWidth = 20;
        public int CellWidth
        {
            get { return cellWidth; }
        }
        private int cellHeight = 20;
        public int CellHeight
        {
            get { return cellHeight; }
        }
        private Font cellFont;
        public Font CellFont
        {
            get
            {
                if (cellFont == null)
                    cellFont = new Font(DefaultFont.FontFamily, 4, FontStyle.Bold);
                return cellFont;
            }
            set { cellFont = value; }
        }
        public float FontSize
        {
            get { return this.CellFont.Size; }
            set { this.CellFont = new Font(this.CellFont.FontFamily, value, this.CellFont.Style); }
        }
        private Dictionary<Rectangle, Color> paletteRects;
        public Dictionary<Rectangle, Color> PaletteRects
        {
            get
            {
                if (paletteRects == null) paletteRects = new Dictionary<Rectangle, Color>();
                return paletteRects;
            }
            set { paletteRects = value; }
        }
        ToolTip t = new ToolTip(new System.ComponentModel.Container());
        public Rectangle LastToolTipReport;

        public PaletteControl()
        {
            InitializeComponent();
            PaletteRects = new Dictionary<Rectangle, Color>();
            paletteBox.MouseMove += paletteBox_MouseMove;
        }

        public void Init()
        {
            PaletteRects = new Dictionary<Rectangle, Color>();
            paletteBox.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        public void paletteBox_MouseMove(object sender, MouseEventArgs e)
        {
            Point mpoint = paletteBox.PointToClient(Control.MousePosition);
            Rectangle mouse = new Rectangle(mpoint, new Size(2, 2));
            // Annoying way to do it, but it stops the flashing and works, so i am over it :D
            for (int i = 0; i < this.PaletteRects.Count; i++)
            {
                if (this.PaletteRects.ElementAt(i).Key.IntersectsWith(mouse))
                {
                    string palEntry = string.Format("Palette Entry({0})", i + 1);
                    if (this.LastToolTipReport == null)
                    {
                        this.LastToolTipReport = this.PaletteRects.ElementAt(i).Key;
                    }
                    else if (this.LastToolTipReport != this.PaletteRects.ElementAt(i).Key)
                    {
                        t.ToolTipTitle = palEntry;
                        t.SetToolTip(paletteBox, this.PaletteRects.ElementAt(i).Value.ToString());
                        this.LastToolTipReport = this.PaletteRects.ElementAt(i).Key;
                    }
                }
            }
        }
        public void ShowPalette(Color[] colors)
        {
         
            if (paletteRects == null) paletteRects = new Dictionary<Rectangle, Color>();
            paletteRects.Clear();
            // drawing 16 squares, 16 times for total of 256 colors
            Bitmap p = new Bitmap(16 * this.CellWidth, 16 * this.CellHeight);
            this.PaletteWidth = p.Width;
            this.PaletteHeight = p.Height;
            Graphics g = Graphics.FromImage(p);
            int x = 0, y = 0;

            for (int i = 0; i < colors.Length; i++)
            {
                SolidBrush b = null;
                // Try to get a color in the palette, if we error, then set us as black.
                try { b = new SolidBrush(colors[i]); }
                catch { b = new SolidBrush(Color.Black); }

                // Create a 25 x 25 rectangle at x, y
                Rectangle r = new Rectangle(x, y, this.CellWidth, this.CellHeight);

                // Draw a white border around it
                g.DrawRectangle(new Pen(new SolidBrush(Color.White)), r);

                // Fill it with our color
                g.FillRectangle(b, x, y, this.CellWidth - 2, this.CellHeight - 2);

                // Now draw the cell number inside the rect, with a font size of 6
                g.DrawString((i + 1).ToString(), this.CellFont, new SolidBrush(Color.Wheat), r);

                // Drop down to the next line of 16, if our next square goes over the width of our palette image.
                if (x + this.CellWidth >= p.Width)
                {
                    x = 0;
                    y += this.CellHeight;
                }
                else x += this.CellWidth;

                // Populate our rectangle dictionary, so we can check to see if the mouse interacts with the cell.
                paletteRects.Add(r, b.Color);
                if (this.OnPaletteLoaded != null) this.OnPaletteLoaded(this.PaletteRects.Values.ToArray());
            }
            this.paletteBox.Image = p;
            this.Refresh();
        }
        public override void Refresh()
        {
            this.Size = new Size(this.PaletteWidth, this.PaletteHeight);
           // this.paletteBox.SizeMode = PictureBoxSizeMode.StretchImage;
            this.paletteBox.Dock = DockStyle.Fill;
            base.Refresh();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SCI32Lib.Stucts
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BITMAPFILEHEADER
    {
        public short bmpType; // string "BM"
        public int bmpSize;
        public short reserved1;
        public short reserved2;
        public int pixelDataOffset;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BITMAPIMAGEHEADER
    {
        public int headerSize; // should be 40
        public int imageWidth;
        public int imageHeight;
        public short imagePlanes; // must be 1
        /// <summary>
        /// 1, 4, 8, 16, 24, 32
        /// </summary>
        public short bitsPerPixel;
        /// <summary>
        /// 0 	Uncompressed
        /// 1 	RLE-8 (Usable only with 8-bit images)
        /// 2 	RLE-4 (Usable only with 4-bit images)
        /// 3 	Bitfields(Used - and required - only with 16- and 32-bit images)
        /// </summary>
        public int imageCompression; //0
        /// <summary>
        /// My be 0 if no compression
        /// </summary>
        public int imageSize;
        public int xPixelsPerMeter;
        public int yPixelsPerMeter;
        public int colorsUsed;
        public int importantColors; // 0 = all

    }



    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BITMAPINFOHEADER
    {
        public uint biSize;
        public int biWidth;
        public int biHeight;
        public ushort biPlanes;
        public ushort biBitCount;
        public uint biCompression;
        public uint biSizeImage;
        public int biXPelsPerMeter;
        public int biYPelsPerMeter;
        public uint biClrUsed;
        public uint biClrImportant;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct RGBQUAD
    {
        /// <summary>
        /// The byte value for blue
        /// </summary>
        public byte blue;
        /// <summary>
        /// The byte value for green
        /// </summary>
        public byte green;
        /// <summary>
        /// The byte value for red
        /// </summary>
        public byte red;
        /// <summary>
        /// The byte value for reserved
        /// </summary>
        public byte res;
    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BITMAPINFO
    {
        public BITMAPFILEHEADER bmiHeader;
        public BITMAPIMAGEHEADER bmiImageHeader;
    }

}

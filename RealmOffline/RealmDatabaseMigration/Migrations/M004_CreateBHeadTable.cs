﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmDatabaseMigration.Migrations {
    [Migration(4)]
    public class M004_CreateBHeadTable: Migration {
        public override void Up() {
            Create.Table("BHead")
                .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                .WithColumn("OwnerId").AsInt32().ForeignKey("WorldObject", "Id").Nullable()
                .WithColumn("Race").AsInt32().Nullable()
                .WithColumn("Sex").AsInt32().Nullable()
                .WithColumn("HeadNumber").AsInt32().Nullable()
                .WithColumn("ChinNumber").AsInt32().Nullable()
                .WithColumn("EyeNumber").AsInt32().Nullable()
                .WithColumn("HairNumber").AsInt32().Nullable()
                .WithColumn("BrowNumber").AsInt32().Nullable()
                .WithColumn("NoseNumber").AsInt32().Nullable()
                .WithColumn("EarNumber").AsInt32().Nullable()
                .WithColumn("MouthNumber").AsInt32().Nullable()
                .WithColumn("FaceHairNumber").AsInt32().Nullable()
                .WithColumn("SkinColor").AsInt32().Nullable()
                .WithColumn("EyeColor").AsInt32().Nullable()
                .WithColumn("HairColor").AsInt32().Nullable();
        }

        public override void Down() {
            Delete.ForeignKey().FromTable("BHead").ForeignColumn("OwnerId").ToTable("WorldObject").PrimaryColumn("Id");
            Delete.Table("BHead");
        }
    }
}

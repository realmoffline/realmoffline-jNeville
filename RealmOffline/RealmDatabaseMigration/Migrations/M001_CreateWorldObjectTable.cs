﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealmDatabaseMigration.Migrations {
    [Migration(1)]
    public class M001_CreateWorldObjectTable: Migration {
        public override void Up() {
            Create.Table("WorldObject")
                .WithColumn("Id").AsInt32().PrimaryKey().Identity()
                .WithColumn("OwnerID").AsInt32().ForeignKey("WorldObject", "Id").Nullable()
                .WithColumn("XPosition").AsInt32().Nullable()
                .WithColumn("YPosition").AsInt32().Nullable()
                .WithColumn("XSpeed").AsInt32().Nullable()
                .WithColumn("YSpeed").AsInt32().Nullable()
                .WithColumn("Color").AsInt32().Nullable()
                .WithColumn("FacingDirection").AsInt32().Nullable()
                .WithColumn("PhysicalState").AsInt32().Nullable()
                .WithColumn("Strength").AsInt32().Nullable()
                .WithColumn("Dexterity").AsInt32().Nullable()
                .WithColumn("Intelligence").AsInt32().Nullable()
                .WithColumn("Quickness").AsInt32().Nullable()
                .WithColumn("Endurance").AsInt32().Nullable()
                .WithColumn("Level").AsInt32().Nullable()
                .WithColumn("Health").AsInt32().Nullable()
                .WithColumn("MaxHealth").AsInt32().Nullable()
                .WithColumn("Alignment").AsInt32().Nullable()
                .WithColumn("Armor").AsInt32().Nullable()
                .WithColumn("BaseArmor").AsInt32().Nullable()
                .WithColumn("Hunger").AsInt32().Nullable()
                .WithColumn("Thirst").AsInt32().Nullable()
                .WithColumn("CreationTime").AsInt32().Nullable()
                .WithColumn("ManaValue").AsInt32().Nullable()
                .WithColumn("RoomNumber").AsInt32().Nullable()
                .WithColumn("ArmorType").AsInt32().Nullable()
                .WithColumn("Stamina").AsInt32().Nullable()

                .WithColumn("IsContainer").AsBoolean().WithDefaultValue(false)
                .WithColumn("IsCharacter").AsBoolean().WithDefaultValue(false)
                .WithColumn("IsInvis").AsBoolean().WithDefaultValue(false)
                .WithColumn("IsHead").AsBoolean().WithDefaultValue(false)
                .WithColumn("IsWearable").AsBoolean().WithDefaultValue(false)
                .WithColumn("IsWeapon").AsBoolean().WithDefaultValue(false);
        }

        public override void Down() {
            Delete.ForeignKey().FromTable("WorldObject").ForeignColumn("OwnerId").ToTable("WorldObject").PrimaryColumn("Id");
            Delete.Table("WorldObject");
        }
    }
}

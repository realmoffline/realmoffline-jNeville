﻿using RealmOffline.Accounts;
using RealmOffline.Data;
using RealmOffline.Managers;
using RealmOffline.Messages;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace RealmOffline.Requests {
    class BeginCharacterCreationRequest : BaseWorldRequest {
        //todo: all this stuff should be in a different class, not in a request :/
        private const byte ADVENTURER = 0x00;
        private const byte WARRIOR = 0x01;
        private const byte WIZARD = 0x02;
        private const byte THIEF = 0x03;

        private readonly Dictionary<byte, int> HEALTH_MAP = new Dictionary<byte, int>() {
            { ADVENTURER, 45 },
            { WARRIOR, 60 },
            { WIZARD, 15 },
            { THIEF, 25 }
        };


        public BeginCharacterCreationRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Tools.SetColor(ConsoleColor.Yellow);
            Console.WriteLine("Creation Packet  48: {0}", BitConverter.ToString(Packets));
            Tools.ResetColor();

            Client.GameAccount.InCharacterCreationProcess = true;
            Character character = GetCharacterFromMessage();
            if (!MySqlManager.IsNameTaken(character.Name)) {
                CharacterCreator.Create(character);
                Client.GameAccount.CurrentCharacter = character;

                Console.WriteLine("AccouuntID : {0}", Client.GameAccount.SqlId);
                byte[] reply = character.CreateNewChar48(Client.GameAccount.SqlId);//Client.GameAccount.GenerateCreateCharPacket();
                string fileLocation = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "MyPacket25(NewChar).txt");
                File.WriteAllText(fileLocation, BitConverter.ToString(reply));
                Console.WriteLine("Sent Char Create 1");

                //looks like no location information is supposed to be here??
                //CreateCharacterCompleteMessage msg = new CreateCharacterCompleteMessage(Client.GameAccount, character);
                //Console.WriteLine(String.Join(",", msg.GetMessageBytes()));
                //Client.Send(msg);
                Client.Send(ref reply);
            }
            else {
                byte[] no = { 0x14, 0x00, 0x00, 0x00, 0x1A, 0x00, 0x00, 0x00, 0x23, 0x0A, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x11, 0x27, 0x00, 0x00, 0x83, 0xCF, 0x49, 0xC4 };
                Client.Send(ref no);
            }
        }

        internal class CharacterCreationMessageData {
            public uint MessageLength { get; private set; }
            public uint Id { get; private set; }
            public string Name { get; private set; }
            public string Title { get; private set; }
            public byte Class { get; private set; }
            public byte Race { get; private set; }
            public byte Gender { get; private set; }
            public byte PvpSwitch { get; private set; }
            public byte Strength { get; private set; }
            public byte Dexterity { get; private set; }
            public byte Intelligence { get; private set; }
            public byte Constitution { get; private set; }
            public byte Alignment { get; private set; }
            public byte Girth { get; private set; }
            public byte Height { get; private set; }

            public CharacterCreationMessageData(byte[] packets) {
                ReadData(packets);
            }

            private void ReadData(byte[] packets) {
                using (var reader = new BinaryReader(new MemoryStream(packets))) {
                    MessageLength = reader.ReadUInt32();
                    Id = reader.ReadUInt32();
                                  
                    ushort uNameLen = BitConverter.ToUInt16(reader.ReadBytes(2), 0);
                    Name = Encoding.ASCII.GetString(reader.ReadBytes(uNameLen));

                    ushort uTtitleLen = BitConverter.ToUInt16(reader.ReadBytes(2), 0);
                    Title = Encoding.ASCII.GetString(reader.ReadBytes(uTtitleLen));

                    Class = reader.ReadByte();
                    Race = reader.ReadByte();
                    Gender = reader.ReadByte();
                    PvpSwitch = reader.ReadByte();
                    Strength = reader.ReadByte();
                    Dexterity = reader.ReadByte();
                    Intelligence = reader.ReadByte();
                    Constitution = reader.ReadByte();
                    Alignment = reader.ReadByte();
                    Girth = reader.ReadByte();
                    Height = reader.ReadByte();
                }
            }
        }

        private Character GetCharacterFromMessage() {
            CharacterCreationMessageData data = new CharacterCreationMessageData(Packets);
            Character character = new Character() {
                Name = data.Name,
                Title = data.Title,
                Class = data.Class,
                Race = data.Race,
                Gender = data.Gender,
                PvpSwitch = data.PvpSwitch,
                Strength = data.Strength,
                Dexterity = data.Dexterity,
                Intelligence = data.Intelligence,
                Constitution = data.Constitution,
                Alignment = data.Alignment,
                Girth = data.Girth,
                Height = data.Height,
                Gold = 10000,
                Mana = 10000,
                Level = 1,
                BuildPoints = 5                
            };

            character.Inventory = character.GenerateNewInventory();
            character.CurrentHP = character.TotalHP = GetDefaultHealth(character);
            character.AccountOwnerId = Client.GameAccount.SqlId;

            return character;
        }

        private int GetDefaultHealth(Character character) {
            return HEALTH_MAP[character.Class];
        }
        
    }
}

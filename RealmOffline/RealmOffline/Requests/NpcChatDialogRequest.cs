﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class NpcChatDialogRequest: BaseWorldRequest {
        public NpcChatDialogRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Reader.ReadBytes(8);
            uint npcId = Reader.ReadUInt32();
            Reader.Close();

            Console.WriteLine("Want NPC ID:{0}'s dialog.", npcId);

            byte[] npc = Packet.NPCDialog(Client.GameAccount);
            Client.Send(ref npc);
        }
    }
}

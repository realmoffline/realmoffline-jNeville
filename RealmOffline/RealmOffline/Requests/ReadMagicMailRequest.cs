﻿using RealmOffline.Managers;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class ReadMagicMailRequest: BaseWorldRequest {
        public ReadMagicMailRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            //      Console.WriteLine("Start 0x88");
            Reader.ReadBytes(8);
            int id = Reader.ReadInt32();
            Reader.Close();
            
            Mail mm = MySqlManager.ReadMail(Client.GameAccount, id);

            if (mm != null) {
                PacketWriter w = new PacketWriter(0x19);
                w.WriteUInt32(Client.GameAccount.AccountId);
                w.WriteInt32(0);
                w.WriteInt32(0x88);
                w.WriteInt32(0);
                byte[] body = Encoding.ASCII.GetBytes(mm.Body);
                w.WriteShort((short)body.Length);
                w.WriteBytes(body);
                w.WriteByte(0x01);
                w.WriteByte(0x01);
                byte[] from = Encoding.ASCII.GetBytes(mm.From);
                w.WriteShort((short)from.Length);
                w.WriteBytes(from);
                w.WriteByte(0x00);
                w.WriteBytes(new byte[] { 0x00, 0x10, 0x89, 0x05 });
                byte[] reply = w.ToArray();
                Client.Send(ref reply);
            }
            else {
                PacketWriter w = new PacketWriter(0x1A);
                w.WriteUInt32(Client.GameAccount.AccountId);
                w.WriteInt32(0);
                w.WriteInt32(0x87);
                w.WriteBytes(new byte[] { 0x14, 0x27 });
                w.WriteShort(0);
                byte[] reply = w.ToArray();

                PacketWriter w1 = new PacketWriter(0x1A);
                w1.WriteUInt32(Client.GameAccount.AccountId);
                w1.WriteInt32(0);
                w1.WriteInt32(0x8A);
                w1.WriteInt32(0);
                byte[] msg = Encoding.ASCII.GetBytes(string.Format("Unable to read email {0}, please refresh and try again.", id));
                w1.WriteShort((short)msg.Length);
                w1.WriteBytes(msg);
                w1.WriteShort(0);
                w1.WriteBytes(new byte[] { 0x00, 0xA8 });
                byte[] reply2 = w1.ToArray();

                Client.Send(ref reply);
                Client.Send(ref reply2);
            }
            //    Console.WriteLine("Sent 0x88");
        }
    }
}

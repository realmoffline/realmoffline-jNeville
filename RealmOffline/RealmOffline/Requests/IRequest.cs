﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests
{
    interface IRequest
    {
        byte[] GetResponse();
    }
}

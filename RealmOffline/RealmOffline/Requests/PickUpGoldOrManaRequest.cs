﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class PickUpGoldOrManaRequest: BaseWorldRequest {
        public PickUpGoldOrManaRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Console.WriteLine("Pickup gold or mana {0}", BitConverter.ToString(Packets));
            Reader.ReadBytes(8);
            uint item = Reader.ReadUInt32();
            Reader.Close();
            PacketWriter w25 = new PacketWriter(0x19);
            w25.WriteUInt32(Client.GameAccount.AccountId);
            w25.WriteUInt32(0x00);
            w25.WriteUInt32(0x4F);
            w25.WriteUInt32(0x00);
            byte[] p25 = w25.ToArray();
            Client.Send(ref p25);



            PacketWriter w42A = new PacketWriter(0x2A);
            w42A.WriteUInt32(Client.GameAccount.AccountId);
            w42A.WriteInt32(0);
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42A.WriteByte(0x1C);
            //w42.WriteUInt32(Client.GameAccount.CurrentCharacter.CharId);
            w42A.WriteUInt32(item);
            // w42.WriteByte(0x21);
            w42A.WriteByte(0xFF);
            byte[] p42A = w42A.ToArray();
            Client.Send(ref p42A);

            // doesnt allow pickup if 42A isnt sent
            PacketWriter w42B = new PacketWriter(0x2A);
            w42B.WriteUInt32(Client.GameAccount.AccountId);
            w42B.WriteUInt32(0x00);
            w42B.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42B.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42B.WriteByte(0x21);
            w42B.WriteByte(0xFF);
            byte[] pw42B = w42B.ToArray();
            Client.Send(ref pw42B);
        }
    }
}

﻿using RealmOffline.Core;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class EnterCombatRequest: BaseWorldRequest {
        public EnterCombatRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Reader.ReadBytes(8);
            uint mob = Reader.ReadUInt32();
            Reader.ReadInt32(); // all FF or -1
            Reader.ReadByte(); // 1 more ff
            ushort mobX = Reader.ReadUInt16();
            ushort mobY = Reader.ReadUInt16();
            ushort mobFace = Reader.ReadUInt16();
            Reader.Close();
            Console.WriteLine("Fight at X:{0} Y:{1} Face:{2}", mobX, mobY, mobFace);
            Mobile m = new Mobile();
            m.GameID = mob;
            m.Location = new RoomLocation(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID, mobX, mobY, mobFace);
            m.Location.CurrentRoom = Client.GameAccount.CurrentCharacter.Location.CurrentRoom;

            #region Combat Cloud Init
            // create the cloud
            uint id = 0;
            if (Client.GameAccount.CurrentCharacter.Cloud == null) {
                Client.GameAccount.CurrentCharacter.Cloud = new CombatCloud();
                Client.GameAccount.CurrentCharacter.Cloud.CloudOwner = Client.GameAccount;
            }
            // add mob to cloud
            Client.GameAccount.CurrentCharacter.Cloud.AddCombatant(m);
            byte[] cl = Client.GameAccount.CurrentCharacter.Cloud.Serialize(out id);
            #endregion
            #region Create Combat Cloud (roompacket)
            PacketWriter cloud = new PacketWriter(0x2A);
            cloud.WriteUInt32(Client.GameAccount.AccountId);
            cloud.WriteUInt32(0x00);
            cloud.WriteUInt32(Client.GameAccount.CurrentCharacter.Cloud.GameID);
            cloud.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            cloud.WriteBytes(cl);
            byte[] pcloud = cloud.ToArray();
            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.SendPacket(pcloud);

            #endregion
            #region Send 25 (Not room)
            PacketWriter w25 = new PacketWriter(0x19);
            w25.WriteUInt32(Client.GameAccount.AccountId);
            w25.WriteUInt32(0x00);
            w25.WriteUInt32(0xA0);
            w25.WriteUInt32(0x00);
            byte[] p25 = w25.ToArray();
            Client.Send(ref p25);
            #endregion
            #region Put us in cloud (roompacket)
            // Now put us in it
            PacketWriter w42A = new PacketWriter(0x2A);
            w42A.WriteUInt32(Client.GameAccount.AccountId);
            w42A.WriteUInt32(0x00);
            // the player to put in it (he who jumped ?)
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42A.WriteByte(0x14); // says next is screen background (left side combatants ?)
            w42A.WriteShort((short)Client.GameAccount.CurrentCharacter.Location.CurrentRoom.Background);
            // the trees on screen, some random way ushort fits with no errors
            w42A.WriteBytes(new byte[] { 0xFF, 0xFF });
            w42A.WriteByte(0x01);
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            // Controlls where on left side the player/mob is
            w42A.WriteByte(0x01); //<---> of left side
            w42A.WriteByte(0x06); // N/S position

            w42A.WriteByte(0x01); // dunno
            w42A.WriteUInt32(mob); // the mob id
                                   // Mob postion ?
            w42A.WriteByte(0x17); //<---> of right side
            w42A.WriteByte(0x06); // N/S position

            w42A.WriteBytes(new byte[] { 0x0B, 0x02, 0x27 }); // cant detect a change
                                                              //w42A.WriteBytes(new byte[] { 0x17, 0x06, 0x0B, 0x02, 0x27 });
            w42A.WriteByte(0x01);
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.Cloud.GameID); // the cloud to put em in
            w42A.WriteByte(0x00);
            w42A.WriteByte(0x15);
            w42A.WriteUInt32(mob);
            w42A.WriteByte(0x21);
            w42A.WriteByte(0xFF);
            byte[] p42A = w42A.ToArray();
            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.SendPacket(p42A);
            #endregion
            #region Init the Cloud (No Room)
            // We are in the cloud now
            PacketWriter w42B = new PacketWriter(0x2A);
            w42B.WriteUInt32(Client.GameAccount.AccountId);
            w42B.WriteUInt32(0x00);
            w42B.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42B.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42B.WriteByte(0x15);
            w42B.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42B.WriteUInt32(0xFF);
            w42B.WriteUInt32(0x00);
            byte[] p42B = w42B.ToArray();
            Client.Send(ref p42B);
            #endregion
            #region Begin Combat (Non Room)
            // Start combat
            PacketWriter w42C = new PacketWriter(0x2A);
            w42C.WriteUInt32(Client.GameAccount.AccountId);
            w42C.WriteUInt32(0x00);
            // init the cloud
            w42C.WriteUInt32(Client.GameAccount.CurrentCharacter.Cloud.GameID);
            w42C.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42C.WriteByte(0x35);
            w42C.WriteUInt32(Client.GameAccount.CurrentCharacter.Cloud.GameID);
            w42C.WriteByte(0xFF);
            byte[] p42C = w42C.ToArray();
            Client.Send(ref p42C);
            #endregion
        }
    }
}

﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class EraseCharacterRequest: BaseWorldRequest {
        public EraseCharacterRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            //   Console.WriteLine("Start 49");
            //  Console.WriteLine("Erase Packet : {0}", BitConverter.ToString(pak));
            // This deletes chars, when you delete a char in front of another char
            // the chars in the slots to the right move up one to the left.

            // 49 is erase a toon packet
            Reader.ReadBytes(4); // packet len
            Reader.ReadBytes(4); // pckid 49

            uint charID = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
            Reader.Close();
            //Character toRemove = acct.GetCharFromGameID(BitConverter.ToUInt16(charID, 0));
            if (Client.GameAccount.RemoveCharByGameID(charID)) {
                // We need to check and move the chars up a slot if one is null
            }



            byte[] reply = Packet.ReplyWith25(Client.GameAccount.AccountId, 49,
                new byte[] { 0x4F, 0x29, 0x0A, 0x08 });
            Client.Send(ref reply);
            //   Console.WriteLine("Sent 49");
        }
    }
}

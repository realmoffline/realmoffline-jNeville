﻿using RealmOffline.Accounts;
using RealmOffline.Core.Items.Base;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class UnEquipItemRequest: BaseWorldRequest {
        public UnEquipItemRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            // Unequip a item
            //      Console.WriteLine("Packet 154 Unequip Item: {0}", BitConverter.ToString(pak));
            Reader.ReadBytes(4); // len
            Reader.ReadBytes(4); //id
            byte[] bslotId = Reader.ReadBytes(4);
            uint slotId = BitConverter.ToUInt32(bslotId, 0);
            Character c = Client.GameAccount.CurrentCharacter;
            // Starts with packet 25
            PacketWriter w25 = new PacketWriter(0x19);
            w25.WriteUInt32(Client.GameAccount.AccountId);
            w25.WriteUInt32(0);
            w25.WriteUInt32(0x9A);
            w25.WriteBytes(new byte[] { 0x65, 0x25, 0x05, 0x08 });
            byte[] p25 = w25.ToArray();
            Client.Send(ref p25);
            BaseGameItem b = Client.GameAccount.CurrentCharacter.GetInventoryItem(slotId);



            byte[] reply = Packet.ChatPacket(1, 1,
                  string.Format("You Unequiped {0}", b.EquipableSlot), "Unequip");
            Client.Send(ref reply);
            //Item i = Managers.MySqlManager.GetItem(Client.GameAccount.CurrentCharacter.SqlCharId, slotId);

            // if (i == null) Console.WriteLine("Fuck bad item grab, wanted {0} for charid {1}.", slotId, Client.GameAccount.CurrentCharacter.SqlCharId);
            Client.GameAccount.CurrentCharacter.Location
                .CurrentRoom.UnequipItemInRoom(Client.GameAccount, slotId);//Packet.UnequipItem(Client.GameAccount, i);
        }
    }
}

﻿using RealmOffline.Core;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class ExitCombatCloudRequest: BaseWorldRequest {
        public ExitCombatCloudRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            PacketWriter w25 = new PacketWriter(0x19);
            w25.WriteUInt32(Client.GameAccount.AccountId);
            w25.WriteUInt32(0x00);
            w25.WriteUInt32(0x45);
            w25.WriteBytes(new byte[] { 0x60, 0x80, 0xE1, 0x1B });
            byte[] p25 = w25.ToArray();
            Client.Send(ref p25);

            PacketWriter w42A = new PacketWriter(0x2A);
            w42A.WriteUInt32(Client.GameAccount.AccountId);
            w42A.WriteUInt32(0x00);
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42A.WriteByte(0x23);
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42A.WriteUShort(150);
            w42A.WriteUShort(150);
            w42A.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42A.WriteByte(0xFF);
            byte[] p42A = w42A.ToArray();
            //Client.Send(ref p42A);
            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.SendPacket(p42A, false);
            PacketWriter w42B = new PacketWriter(0x2A);
            w42B.WriteUInt32(Client.GameAccount.AccountId);
            w42B.WriteUInt32(0x00);
            // cloud
            w42B.WriteUInt32(Client.GameAccount.CurrentCharacter.Cloud.GameID);
            w42B.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42B.WriteByte(0x3E);
            w42B.WriteUInt32(Client.GameAccount.CurrentCharacter.Cloud.GameID);
            w42B.WriteBytes(new byte[] { 0xAC, 0x91, 0x00, 0x00 });
            w42B.WriteByte(0xFF);
            byte[] p42B = w42B.ToArray();
            //Client.Send(ref p42B);
            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.SendPacket(p42B, false);

            PacketWriter w45 = new PacketWriter(0x45);
            w45.WriteUInt32(Client.GameAccount.AccountId);
            w45.WriteUInt32(0x00);
            w45.WriteBytes(new byte[] { 0x00, 0x0B, 0x02, 0x27 });
            w45.WriteByte(0x01);
            w45.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            byte[] p45 = w45.ToArray();
            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.SendPacket(p45, false);

            // Get all the mobs in cloud that died.
            Mobile m = Client.GameAccount.CurrentCharacter.Cloud.Combatants[0];

            PacketWriter w42C = new PacketWriter(0x2D);
            w42C.WriteUInt32(Client.GameAccount.AccountId);
            w42C.WriteUInt32(0x00);
            w42C.WriteInt32((int)Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            // cloud
            w42C.WriteUInt32(Client.GameAccount.CurrentCharacter.Cloud.GameID);
            w42C.WriteUInt32(Client.GameAccount.CurrentCharacter.Cloud.GameID);
            w42C.WriteBytes(new byte[] { 0x00, 0xD1, 0x05, 0xFB });
            byte[] p42C = w42C.ToArray();
            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.SendPacket(p42C, false);

            // now remove the dead mob(s)
            PacketWriter w42D = new PacketWriter(0x2D);
            w42D.WriteUInt32(Client.GameAccount.AccountId);
            w42D.WriteUInt32(0x00);
            w42D.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42D.WriteUInt32(m.GameID);
            w42D.WriteBytes(new byte[] { 0x00, 0x00, 0x00, 0x56 });
            byte[] p42D = w42D.ToArray();
            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.SendPacket(p42D, false);
            // remove mob(s) from room
            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RemoveEntity(m, Client.GameAccount);
            // Combat done
            Client.GameAccount.CurrentCharacter.Cloud.Combatants.Clear();
        }
    }
}

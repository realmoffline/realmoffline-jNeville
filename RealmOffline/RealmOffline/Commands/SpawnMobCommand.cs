﻿using RealmOffline.Base;
using RealmOffline.Core;
using RealmOffline.Core.Items.Base;
using RealmOffline.Core.Mobiles;
using RealmOffline.Messages;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using RealmOffline.TCP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Commands {
    class SpawnMobCommand: BaseCommand {
        private uint entityId;
        private uint EntityId {
            get {
                if (entityId == default(uint)) {
                    entityId = ServerGlobals.GetNextAvailableID();
                    Client.GameAccount.CurrentCharacter.IDsInUse.Add(entityId);
                }
                
                return entityId;
            }
        }

        private short mobId;
        private short MobId {
            get {
                if (mobId == default(short)) {
                    short.TryParse(CommandArgs[1], out mobId);
                }

                return mobId;
            }
        }

        private MobileType mobType;
        private MobileType MobType {
            get {
                if (mobType == default(MobileType)) {                    
                    BaseMobile.MobNameFromID(MobId, out mobType);
                }
                return mobType;
            }
        }

        private string mobName;
        private string MobName {
            get {
                if (mobName == null) {
                    MobileType type = MobileType.None;
                    mobName = BaseMobile.MobNameFromID(MobId, out type);
                }
                return mobName;
            }
        }

        public SpawnMobCommand(WorldClient client, byte[] packets) : base(client, packets) {}
        
        public override void SendResponse() {
            RoomLocation r = new RoomLocation(
                Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID,
                Client.GameAccount.CurrentCharacter.Location.X,
                Client.GameAccount.CurrentCharacter.Location.Y,
                Client.GameAccount.CurrentCharacter.Location.Facing
            );

            Mobile m = GetMobile();
            m.GraphicID = (ushort)MobId;
            m.GameID = EntityId;
            m.Location = r;            
            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.AddEntity(m, Client.GameAccount);

            AddToRoomMessage message = new AddToRoomMessage(Client.GameAccount, m);
            RoomBroadcaster.SendToRoom(r.CurrentRoom, message);

            byte[] reply = Packet.ChatPacket(1, 1, string.Format("Created {0} mob {1} with a GameID of:({2}).", MobName, MobId, EntityId), "Npc");
            Console.WriteLine("Sending chat reply to account {0}, char id {1}", Client.GameAccount.AccountId, Client.GameAccount.CurrentCharacter.GameID);
            Client.Send(ref reply);
        }

        private Mobile GetMobile() {
            return MobType == MobileType.NonHumanoid ? GetMonster() : GetNPC();
        }

        private Mobile GetMonster() {
            Mob m = new Mob();            
            m.Girth = 100;
            m.Height = 100;
            m.CurrentHitPoints = 100;
            m.MaxHitPoints = 100;
            m.MobBrain = new Brain(m);

            return m;
        }

        private Mobile GetNPC() {
            NPC m = new NPC();
            m.Girth = 100;
            m.Height = 100;
            m.CurrentHitPoints = 1201230;
            m.MaxHitPoints = 1201230;
            m.Gender = 1;
            m.RaceHead = 1;
            m.ChinPart = 0;
            m.EarPart = 0;
            m.EyeBrowPart = 0;
            m.EyeColor = 2;
            m.EyePart = 0;
            m.FacialHairPart = 0;
            m.HairColor = 2;
            m.HairPart = 0;
            m.HeadPart = 0;
            m.MouthPart = 0;
            m.NosePart = 0;
            m.RaceHead = 1;
            m.SkinColor = 2;
            m.Inventory = GetInventory();
            m.CreateFace();
            return m;
        }

        private List<BaseGameItem> GetInventory() {
            List<BaseGameItem> defaultInventory = new List<BaseGameItem>();
            Item i1 = new Item();
            i1.GameID = ServerGlobals.GetNextAvailableID();
            i1.Equipped = true;
            i1.Color = 99;
            i1.GraphicID = 1155;
            defaultInventory.Add(i1);
            i1 = new Item();
            i1.GameID = ServerGlobals.GetNextAvailableID();
            i1.Equipped = true;
            i1.Color = 99;
            i1.GraphicID = 1191;
            defaultInventory.Add(i1);

            return defaultInventory;
        }
    }
}

﻿using RealmOffline.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    class MoveToNextScreenMessage : BaseMessage {
        public MoveToNextScreenMessage(Account account) : base(account, Requests.RequestTypes.MoveToNextScreen) { }

        protected override void WritePackets() { }
    }
}

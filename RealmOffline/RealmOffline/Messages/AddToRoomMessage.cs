﻿using RealmOffline.Accounts;
using RealmOffline.Base;
using RealmOffline.Core;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    class AddToRoomMessage : MovieDataMessage {
        private IEntity Entity { get; }
        public AddToRoomMessage(Account account, IEntity entityToAdd) : base(account, MovieTypes.CreateCharacter) {
            Entity = entityToAdd;
        }

        protected override void WritePackets() {
            base.WritePackets();

            if (Entity is Mobile)
                Packets.WriteBytes(Packet.WrappedMob(Entity as Mobile));
            else if (Entity is Character) {
                Character character = Entity as Character;
                EntityInfoMessagePart characterInfo = new EntityInfoMessagePart(character, Room);
                Packets.WriteBytes(characterInfo.GetMessageBytes());
            }
                
        }
    }
}

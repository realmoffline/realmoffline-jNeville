﻿using RealmOffline.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    public class AckMessage : BaseMessage {
        public Requests.RequestTypes AckType { get; }
        public AckMessage(Account account, Requests.RequestTypes ackType) : base(account, Requests.RequestTypes.Ack) {
            AckType = ackType;
        }

        protected override void WritePackets() {
            base.WritePackets();

            Packets.WriteByte((byte)AckType);
        }
    }
}

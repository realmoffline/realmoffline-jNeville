﻿using RealmOffline.Accounts;
using RealmOffline.Base;
using RealmOffline.Core.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    class RemoveFromRoomMessage: BaseMessage {
        private Character RemovedCharacter { get; }
        private Room FromRoom { get; }
        public RemoveFromRoomMessage(Account account, Room fromRoom,  Character removedCharacter) : base(account, Requests.RequestTypes.DestroyObject) {
            RemovedCharacter = removedCharacter;
            FromRoom = fromRoom;
        }

        protected override void WritePackets() {
            base.WritePackets();
            Packets.WriteUInt32(FromRoom.RoomID);
            Packets.WriteUInt32(RemovedCharacter.GameID); // obj to remove
        }
    }
}

﻿using RealmOffline.Base;
using RealmOffline.Core;
using RealmOffline.Core.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Messages {
    class TeleportMovieMessage : MovieDataMessage {
        public IEntity Entity { get; }
        public Room NewRoom { get; }
        public TeleportMovieMessage(Account account, IEntity entity, Room oldRoom, Room newRoom) : base(account, MovieTypes.Teleport) {
            Entity = entity;
            Room = oldRoom;
            NewRoom = newRoom;
        }

        protected override void WritePackets() {
            base.WritePackets();

            Packets.WriteUInt32(Account.CurrentCharacter.GameID);
            Packets.WriteUInt32(Account.CurrentCharacter.GameID);
            Packets.WriteUInt32(NewRoom.RoomID);
            Packets.WriteByte((byte)MovieTypes.EndMovie);
        }
    }
}

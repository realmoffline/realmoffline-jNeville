﻿using RealmOffline.Managers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace RealmOffline {
    public static class DatabaseUpdater {
        public static void Update() {
            try {
                Migrate();
            }
            catch {
                Migrate("-t rollback");
            }
        }

        public static void RollbackAll() {
            Migrate("-t rollback:all");
        }

        private static void Migrate(string commandLineArgs = "") {
            XMLManager xml = new XMLManager();
            Process migrator = new Process();
            migrator.StartInfo = new ProcessStartInfo() {
                FileName = @"..\..\packages\FluentMigrator.1.6.2\tools\Migrate.exe",
                Arguments = String.Format("/connection \"{0}\" /db mysql /target ./RealmDatabaseMigration.dll {1}", xml.GetMySqlConnection, commandLineArgs),
                UseShellExecute = false,
                RedirectStandardOutput = true
            };

            migrator.OutputDataReceived += (sender, args) => Console.WriteLine("Migrator: {0}", args.Data);
            migrator.Start();
            migrator.BeginOutputReadLine();
            migrator.WaitForExit();
            migrator.CancelOutputRead();


            if (migrator.ExitCode != 0)
                throw new Exception("Migration Error!");

        }
    }
}

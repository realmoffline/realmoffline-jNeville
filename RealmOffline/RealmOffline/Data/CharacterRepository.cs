﻿using MySql.Data.MySqlClient;
using RealmOffline.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Data {
    class CharacterRepository : MySqlTransactionBase {

        #region static interface
        public static List<Character> LoadCharactersForAccount(uint accountId) {
            return new CharacterRepository().GetByAccount(accountId);
        }

        public static Character GetCharacterById(uint worldObjectId) {
            return new CharacterRepository().GetById(worldObjectId);
        }
        #endregion

        private Character GetById(uint worldObjectId) {
            Character character = null;
            try {
                string query = @"
                SELECT * 
                  FROM BCharacter
             LEFT JOIN WorldObject on BCharacter.WorldObjectId = WorldObject.id
             LEFT JOIN BHead on BCharacter.WorldObjectId = BHead.WorldObjectId
                 WHERE BCharacter.WorldObjectId=@id
                ";

                MySqlCommand cmd = new MySqlCommand(query);
                cmd.Parameters.AddWithValue("@id", worldObjectId);

                MySqlDataReader read = cmd.ExecuteReader();
                if (read.Read()) {
                    character = DeserializeCharacter(read);
                }   
            }
            finally {
                CloseConnection();
            }
            return character;
        }

        private List<Character> GetByAccount(uint accountId) {
            var reply = new List<Character>();
            try {
                string query = @"
                SELECT * 
                  FROM BCharacter
             LEFT JOIN WorldObject on BCharacter.WorldObjectId = WorldObject.id
             LEFT JOIN BHead on BCharacter.WorldObjectId = BHead.WorldObjectId
                 WHERE AccountID=@accountId
                ";

                MySqlCommand cmd = new MySqlCommand(query);
                cmd.Parameters.AddWithValue("@accountId", accountId);

                using (MySqlDataReader read = GetReader(cmd)) {
                    while (read.Read()) {
                        var character = DeserializeCharacter(read);
                        reply.Add(character);
                    }
                }
            }
            finally {
                CloseConnection();
            }
            return reply;
        }

        private Character DeserializeCharacter(MySqlDataReader read) {
            var character = new Character();
            character.SqlCharId = GetColumnValue<uint>(read, "WorldObjectId");
            //character.CharId = read.GetUInt32("CurrentID");
            character.AccountOwnerId = GetColumnValue<uint>(read, "AccountID");
            character.Name = GetColumnValue<string>(read, "Name");
            character.Class = GetColumnValue<byte>(read, "Profession");
            character.Race = GetColumnValue<byte>(read, "Race"); //missing column
            character.Gender = GetColumnValue<byte>(read, "Sex");
            character.Strength = GetColumnValue<byte>(read, "Strength");
            character.Dexterity = GetColumnValue<byte>(read, "Dexterity");
            character.Intelligence = GetColumnValue<byte>(read, "Intelligence");
            character.Constitution = GetColumnValue<byte>(read, "Endurance");
            character.Alignment = GetColumnValue<byte>(read, "Alignment");
            //character.Invis = (byte)read.GetUInt16("Invis"); //wot?
            character.Girth = GetColumnValue<byte>(read, "Girth");
            character.Height = GetColumnValue<byte>(read, "Height");
            character.RaceHead = GetColumnValue<byte>(read, "Race");
            character.HeadPart = GetColumnValue<byte>(read, "HeadNumber");
            //character.ChinPart = (byte)read.GetUInt16("BHead.ChinPart"); //wot is dis??
            character.HairPart = GetColumnValue<byte>(read, "HairNumber");
            character.EyeBrowPart = GetColumnValue<byte>(read, "BrowNumber");
            character.EyePart = GetColumnValue<byte>(read, "EyeNumber");
            character.NosePart = GetColumnValue<byte>(read, "NoseNumber");
            character.EarPart = GetColumnValue<byte>(read, "EarNumber");
            character.MouthPart = GetColumnValue<byte>(read, "MouthNumber");
            character.FacialHairPart = GetColumnValue<byte>(read, "FaceHairNumber");
            character.SkinColor = GetColumnValue<byte>(read, "SkinColor");
            character.HairColor = GetColumnValue<byte>(read, "HairColor");
            character.EyeColor = GetColumnValue<byte>(read, "EyeColor");
            character.Title = GetColumnValue<string>(read, "Title");
            character.PvpSwitch = GetColumnValue<byte>(read, "Peaceful");
            character.Biography = "Not Working Yet";// read.GetString("Biography");
            character.Gold = 0; //read.GetInt32("Gold"); //not working yet
            character.Mana = 0; // read.GetInt32("Mana"); //not working yet
            character.Level = GetColumnValue<ushort>(read, "Level");
            character.CurrentHP = GetColumnValue<int>(read, "Health");
            character.TotalHP = GetColumnValue<int>(read, "MaxHealth");
            character.BuildPoints = GetColumnValue<ushort>(read, "BuildPoints");
            character.CurrentXP = GetColumnValue<int>(read, "Experience");
            character.CharDataAdded = true;
            character.PartDataAdded = true;
            character.DescriptionAdded = true;

            return character;
        }

    }
}


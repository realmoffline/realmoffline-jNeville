﻿using MySql.Data.MySqlClient;
using RealmOffline.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Data {
    class CharacterUpdater: MySqlTransactionBase {
        private Character Character { get; set; }
        public CharacterUpdater(Character character) {
            Character = character;
        }

        public static void Update(Character character) {
            CharacterUpdater updater = new CharacterUpdater(character);
            updater.Update();
        }

        public void Update() {
            try {
                UpdateWorldObject();
                UpdateBCharacter();
                UpdateBHead();
                //todo: update inventory here as well (probably??)
                CommitTransaction();
            }
            catch (Exception e) {
                RollbackTransaction();
                throw e;
            }
        }

        private void UpdateBCharacter() {
            string sql = @"
                 UPDATE BCharacter
                    SET Name = @Name,
                        Title = @Title,
                        Profession = @Profession,
                        Experience = @Experience,
                        Sex = @Sex,
                        BuildPoints = @BuildPoints,
                        HomeDown = @HomeDown,
                        StealingCount = @StealingCount,
                        KillingCount = @KillingCount,
                        StealingUnserved = @StealingUnserved,
                        KillingUnserved = @KillingUnserved,
                        Peaceful = @Peaceful,
                        QuestNumber = @QuestNumber,
                        QuestState = @QuestState,
                        WarnCount = @WarnCount,
                        PlayerKills = @PlayerKills,
                        NpcKills = @NpcKills,
                        AttackSkill = @AttackSkill,
                        DodgeSkill = @DodgeSkill,
                        NumAttacks = @NumAttacks,
                        NumDodges = @NumDodges,
                        NumBlocks = @NumBlocks,
                        NumMoves = @NumMoves,
                        CanRetaliate = @CanRetaliate,
                        CombatRound = @CombatRound,
                        SoundGroup = @SoundGroup,
                        AccountId = @AccountId,
                        Race = @Race
                 WHERE WorldObjectId = @Id
            ";

            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@Id", Character.SqlCharId);
            cmd.Parameters.AddWithValue("@Name", Character.Name);
            cmd.Parameters.AddWithValue("@Title", Character.Title);
            cmd.Parameters.AddWithValue("@Profession", Character.Class);
            cmd.Parameters.AddWithValue("@Experience", Character.Experience);
            cmd.Parameters.AddWithValue("@Sex", Character.Gender);
            cmd.Parameters.AddWithValue("@BuildPoints", Character.BuildPoints);
            cmd.Parameters.AddWithValue("@HomeDown", 0); //lol typos :|
            cmd.Parameters.AddWithValue("@StealingCount", 0); //todo fix
            cmd.Parameters.AddWithValue("@KillingCount", 0); //todo fix
            cmd.Parameters.AddWithValue("@StealingUnserved", 0); //todo fix
            cmd.Parameters.AddWithValue("@KillingUnserved", 0); //todo fix
            cmd.Parameters.AddWithValue("@Peaceful", Character.PvpSwitch);
            cmd.Parameters.AddWithValue("@QuestNumber", 0);
            cmd.Parameters.AddWithValue("@QuestState", 0);
            cmd.Parameters.AddWithValue("@WarnCount", 0);
            cmd.Parameters.AddWithValue("@PlayerKills", 0);
            cmd.Parameters.AddWithValue("@NpcKills", 0);
            cmd.Parameters.AddWithValue("@AttackSkill", 0);
            cmd.Parameters.AddWithValue("@DodgeSkill", 0);
            cmd.Parameters.AddWithValue("@NumAttacks", 0);
            cmd.Parameters.AddWithValue("@NumDodges", 0);
            cmd.Parameters.AddWithValue("@NumBlocks", 0);
            cmd.Parameters.AddWithValue("@NumMoves", 0);
            cmd.Parameters.AddWithValue("@CanRetaliate", 0);
            cmd.Parameters.AddWithValue("@CombatRound", 0);
            cmd.Parameters.AddWithValue("@SoundGroup", 0);
            cmd.Parameters.AddWithValue("AccountId", Character.AccountOwnerId);
            cmd.Parameters.AddWithValue("@Race", Character.Race);

            ExecuteInTransaction(cmd);
        }

        private void UpdateWorldObject() {
            string sql = @"
                 UPDATE WorldObject
                    SET XPosition = @XPosition,
                        YPosition = @YPosition,
                        XSpeed = @XSpeed,
                        Color = @Color,
                        FacingDirection = @FacingDirection,
                        Strength = @Strength,
                        Dexterity = @Dexterity,
                        Intelligence = @Intelligence,
                        Endurance = @Endurance,
                        Level = @Level,
                        Health = @Health,
                        MaxHealth = @MaxHealth,
                        Alignment = @Alignment,
                        Armor = @Armor,
                        BaseArmor = @BaseArmor,
                        Hunger = @Hunger,
                        Thirst = @Thirst,
                        ManaValue = @ManaValue,
                        RoomNumber = @RoomNumber,
                        ArmorType = @ArmorType,
                        Stamina = @Stamina,
                        Height = @Height,
                        Girth = @Girth,
                        Quickness = @Quickness
                 WHERE Id = @Id
            ";

            

            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@Id", Character.SqlCharId);
            cmd.Parameters.AddWithValue("@XPosition", Character.Location != null ? Character.Location.X : 0);
            cmd.Parameters.AddWithValue("@YPosition", Character.Location != null ? Character.Location.Y : 0);
            cmd.Parameters.AddWithValue("@XSpeed", Character.XSpeed);
            cmd.Parameters.AddWithValue("@YSpeed", Character.YSpeed);
            cmd.Parameters.AddWithValue("@Color", Character.Color);
            cmd.Parameters.AddWithValue("@FacingDirection", Character.Location != null ? Character.Location.Facing : 0);
            cmd.Parameters.AddWithValue("@Strength", Character.Strength);
            cmd.Parameters.AddWithValue("@Dexterity", Character.Dexterity);
            cmd.Parameters.AddWithValue("@Intelligence", Character.Intelligence);
            cmd.Parameters.AddWithValue("@Endurance", Character.Constitution);
            cmd.Parameters.AddWithValue("@Level", Character.Level);
            cmd.Parameters.AddWithValue("@Health", Character.CurrentHP);
            cmd.Parameters.AddWithValue("@MaxHealth", Character.TotalHP);
            cmd.Parameters.AddWithValue("@Alignment", Character.Alignment);
            cmd.Parameters.AddWithValue("@Quickness", 0); //todo fix
            cmd.Parameters.AddWithValue("@Armor", 0); // todo fix
            cmd.Parameters.AddWithValue("@BaseArmor", 0); //todo fix
            cmd.Parameters.AddWithValue("@Hunger", 0); //todo fix
            cmd.Parameters.AddWithValue("@Thirst", 0); //todo fix
            cmd.Parameters.AddWithValue("@ManaValue", Character.Mana);
            cmd.Parameters.AddWithValue("@RoomNumber", Character.Location != null ? Character.Location.CurrentRoom.RoomID : 0);
            cmd.Parameters.AddWithValue("@ArmorType", 0); //todo fix
            cmd.Parameters.AddWithValue("@Stamina", 0); //todo fix
            cmd.Parameters.AddWithValue("@Height", Character.Height);
            cmd.Parameters.AddWithValue("@Girth", Character.Girth);

            ExecuteInTransaction(cmd);
        }

        private void UpdateBHead() {
            string sql = @"
                 UPDATE BHead
                    SET Race = @Race,
                        Sex = @Sex,
                        HeadNumber = @HeadNumber,
                        ChinNumber = @ChinNumber,
                        EyeNumber = @EyeNumber,
                        HairNumber = @HairNumber,
                        BrowNumber = @BrowNumber,
                        NoseNumber = @NoseNumber,
                        EarNumber = @EarNumber,
                        MouthNumber = @MouthNumber,
                        FaceHairNumber = @FaceHairNumber,
                        SkinColor = @SkinColor,
                        EyeColor = @EyeColor,
                        HairColor = @HairColor
                 WHERE WorldObjectId = @Id
            ";

            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@Id", Character.SqlCharId);
            cmd.Parameters.AddWithValue("@Race", Character.RaceHead);
            cmd.Parameters.AddWithValue("@Sex", Character.Gender);
            cmd.Parameters.AddWithValue("@HeadNumber", Character.HeadPart);
            cmd.Parameters.AddWithValue("@ChinNumber", Character.ChinPart);
            cmd.Parameters.AddWithValue("@EyeNumber", Character.EyePart);
            cmd.Parameters.AddWithValue("@HairNumber", Character.HeadPart);
            cmd.Parameters.AddWithValue("@BrowNumber", Character.EyeBrowPart);
            cmd.Parameters.AddWithValue("@NoseNumber", Character.NosePart);
            cmd.Parameters.AddWithValue("@EarNumber", Character.EarPart);
            cmd.Parameters.AddWithValue("@MouthNumber", Character.MouthPart);
            cmd.Parameters.AddWithValue("@FaceHairNumber", Character.FacialHairPart);
            cmd.Parameters.AddWithValue("@SkinColor", Character.SkinColor);
            cmd.Parameters.AddWithValue("@EyeColor", Character.EyeColor);
            cmd.Parameters.AddWithValue("@HairColor", Character.HairColor);

            ExecuteInTransaction(cmd);
        }

    }
}

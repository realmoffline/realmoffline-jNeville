﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOfflineRouter.Requests {
    class HelloWorldRequest: BaseRouterRequest {
        public HelloWorldRequest(byte[] packets) : base(packets) { }

        public override byte[] GetResponse() {
            return new byte[]{
                0x18, 0x00, 0x00, 0x00, // 18 first byte in hex, is 24 in dec, length of this packet
                0x19, 0x00, 0x00, 0x00, // Header ? is 7 // hex 19 = 25 the op code ?

                0x00, 0x00, 0x00, 0x00, // Starts the actual hello packet
                0x00, 0x00, 0x00, 0x00,
                0x06, 0x00, 0x00, 0x00,
                0x0a, 0x02, 0x00, 0x00,
                0x00, 0x85, 0x5e, 0xf2,
                0x02, 0x60, 0x00, 0x05
            };
        }
    }
}

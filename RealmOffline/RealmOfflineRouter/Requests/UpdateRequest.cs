﻿using RealmOffline.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOfflineRouter.Requests {
    class UpdateRequest: BaseRouterRequest {
        public UpdateRequest(byte[] packets) : base(packets) { }

        public override byte[] GetResponse() {
            byte[] response = new byte[0];
            PacketReader reader = new PacketReader(Packets);
            reader.ReadInt32(); // pkt length
            reader.ReadInt32(); // pkt id
            ushort cmd_len = reader.ReadUInt16();
            string cmd = reader.ReadString(cmd_len);
            reader.ReadByte();
            reader.ReadInt32();
            reader.Close();


            // "updates 3 522"
            // "gamelist"
            string[] cmdArray = cmd.Split(' ');
            switch (cmdArray[0].ToLower()) {
                case "updates": {
                        Console.WriteLine("Client version {0}.{1}", cmdArray[1], cmdArray[2]);
                        response = new PatchInformationRequest(Packets).GetResponse();
                    }
                    break;
                case "gamelist": {
                        response = new GameListRequest(Packets).GetResponse();
                    }
                    break;
                default:
                    Console.WriteLine("RouterClient:: Unknown command ({0})", cmd);
                    break;
            }

            return response;
        }
    }
}

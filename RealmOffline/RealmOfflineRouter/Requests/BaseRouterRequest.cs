﻿using RealmOffline.Packets;
using RealmOffline.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOfflineRouter.Requests {
    public abstract class BaseRouterRequest : BaseRequest {
        public BaseRouterRequest(byte[] packets) : base(packets) { }

        public void WriteHeader() {
            PacketWriter.WriteInt32(25);
            PacketWriter.WriteInt32(0);
            PacketWriter.WriteInt32(0);
            PacketWriter.WriteInt32(100);
            PacketWriter.WriteInt32(0);
        }        
    }
}
